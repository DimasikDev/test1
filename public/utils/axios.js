import axios from "axios";

export default axios.create({
    baseURL: "https://stage.swapmap.app/api/v1/",
    responseType: "json"
});

