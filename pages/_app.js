import '../styles/main.css'
import { useRouter } from 'next/router'

function MyApp({Component, props}) {
    const router = useRouter()
    return <Component router={router} {...props} />
}

export default MyApp
