import Head from 'next/head'
import {Component} from "react";
import 'selectize/dist/css/selectize.css';
import $ from 'jquery';
import {Main} from "../main";
import axios from "../../public/utils/axios";

export class Login extends Component {
    render() {
        return (
            <div>
                <Head>
                    <title>SwapMap</title>
                </Head>
                <Main/>
                <div className="container main">
                    <div className="container-sm login-container">
                        <div className="card login-card">
                            <img className="mb-6" src="/Logo%20large.png"
                                 style={{objectFit: 'contain', maxHeight: '92px'}}/>
                            <div className="card-body">
                                    <div className="form-group mb-3">
                                        <label className="form-label" htmlFor="country">Выберите страну</label>
                                        <select defaultValue={'7'} style={{display: 'none'}} id="country"
                                                aria-label="Default select example">
                                            <option value="7" selected>Россия</option>
                                        </select>
                                    </div>
                                    <div className="form-group mb-3">
                                        <label className="form-label">Введите номер телефона</label>
                                        <div className="d-flex">
                                            <div className="form-control mr-1 disabled">
                                                <input id="tel-input-code" value="+7" disabled
                                                       style={{width: '84px', textAlign: 'center'}}
                                                       className="form-control"/>
                                            </div>
                                            <div className="form-control">
                                                <input type="tel" id="tel-input" className="form-control"
                                                       placeholder="999-999-999"/>
                                            </div>
                                        </div>
                                    </div>
                                    <button id="continue-button" className="btn button large" disabled>Продолжить
                                    </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    componentDidMount() {
        require('selectize')
        require('imask')
        let router = this.props.router;
        $(function () {
            let focusedInput

            $('#country').on('change', function (e) {
                $('#tel-input-code')[0].value = '+' + e.target.value
            })

            $('#tel-input').on('focus', function (e) {
                $('#tel-input-code')[0].classList.add('active')
            })

            $('#tel-input').on('blur', function (e) {
                if (!$('#tel-input').is(':hover')) {
                    $('#tel-input-code')[0].classList.remove('active')
                }
            })

            $('#tel-input').on('mouseenter', function (e) {
                $('#tel-input-code')[0].classList.add('active')
            })

            $('#tel-input').on('mouseleave', function (e) {
                if (!$('#tel-input').is(':focus')) {
                    $('#tel-input-code')[0].classList.remove('active')
                }
            })

            $('input').on('focus', function (e) {
                if ($(e.currentTarget).attr('disabled')) return
                let button = document.createElement('div')
                button.classList = 'clear-button'
                focusedInput = $(e.currentTarget)
                button.addEventListener('click', function (e) {
                    e.preventDefault()
                    e.stopPropagation()
                    focusedInput[0].value = ''
                    focusedInput.trigger('input')
                })
                e.currentTarget.parentElement.append(button)
            })
            $('input').on('blur', function (e) {
                e.preventDefault()
                e.stopPropagation()
                setTimeout(() => $(e.currentTarget.parentElement).find('.clear-button').remove(), 50)
            })

            $('#country').selectize({
                valueField: 'value',
                labelField: 'name',
                options: [
                    {value: '7', name: 'Россия', code: '+7'},
                    {value: '71', name: 'Россия', code: '+71'},
                    {value: '72', name: 'Россия', code: '+72'},
                    {value: '73', name: 'Беларусь', code: '+73'},
                    {value: '74', name: 'Беларусь', code: '+74'},
                    {value: '75', name: 'Беларусь', code: '+75'},
                    {value: '76', name: 'Беларусь', code: '+76'},
                    {value: '77', name: 'Украина', code: '+77'},
                    {value: '78', name: 'Украина', code: '+78'},
                    {value: '79', name: 'Украина', code: '+79'},
                    {value: '70', name: 'Украина', code: '+70'},
                ],
                create: false,
                render: {
                    option: function (item, escape) {
                        return `<div class="option">` + item.name +
                            '<div>' + item.code + '</div> ' +
                            '</div>';
                    }
                },
                load: function (query, callback) {
                    callback([
                        {value: '7', name: 'Россия', code: '+71'},
                        {value: '8', name: 'Россия', code: '+72'},
                        {value: '9', name: 'Россия', code: '+73'},
                        {value: '71', name: 'Беларусь', code: '+74'},
                        {value: '711', name: 'Беларусь', code: '+75'},
                        {value: '72', name: 'Беларусь', code: '+76'},
                        {value: '73', name: 'Беларусь', code: '+77'},
                        {value: '74', name: 'Украина', code: '+78'},
                        {value: '75', name: 'Украина', code: '+79'},
                        {value: '77', name: 'Украина', code: '+70'},
                        {value: '78', name: 'Украина', code: '+722'},
                    ])
                }
            });
            let opacityLayer = document.createElement('div');
            opacityLayer.classList = 'opacity-layer'
            opacityLayer.id = 'opacity-layer'
            let dropdown = $('.selectize-dropdown-content');
            dropdown[0].parentElement.style.opacity = 0
            $('.selectize-input').click()
            setTimeout(() => {
                dropdown[0].parentElement.style.display = 'block'
                if (dropdown[0].scrollHeight > dropdown[0].clientHeight) {
                    opacityLayer.classList.add('overflow')
                }
                dropdown[0].parentElement.style.display = 'none'
                dropdown[0].parentElement.style.opacity = 1
            }, 1000)
            dropdown.before(opacityLayer)
            let element = $('#tel-input')[0];
            var mask = IMask(element, {
                mask: '000-000-000'
            });
            let oldValue = '';

            let button = $('#continue-button');
            mask.on('complete', () => {
                setTimeout(() => {
                    button.removeAttr('disabled')
                }, 10)
            })

            $(element).on('input', function () {
                if (element.value !== oldValue) {
                    button.attr('disabled', 'disabled')
                    oldValue = element.value
                }
            })

            button.on('click', function (e) {
                button.attr('disabled', 'disabled')
                e.preventDefault()
                e.stopPropagation()
                let phone = $('#country')[0].value + $('#tel-input')[0].value.replaceAll('-', '');
                $('#login-form').append($.parseHTML(`<input type="hidden" name="phone" id="actual-tel-input" value="${phone}" />`))
                axios.post('users/register?step=1', {phone, accept_offer: true}).then(function () {
                    router.push('/login/sms?phone=' + phone).then(() => {
                        button.removeAttr('disabled')
                    })
                })
                    .catch((e) => {
                        button.removeAttr('disabled')
                    })
            });
        })
    }
}

export default Login
